/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.nmea;

import eu.hansolo.nmea.NmeaManager.*;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Enumeration;


/**
 * User: hansolo
 * Date: 29.01.15
 * Time: 11:11
 */
public class Main implements SerialPortEventListener {
    private static final String PORT       = "/dev/cu.usbserial-DJ005JJS"; //"/dev/cu.usbmodem1421"; //Set the COM port here
    private static final int    TIME_OUT   = 2000;
    private static final int    DATA_RATE  = 9600;
    private SerialPort          serialPort;
    private BufferedReader      input;


    // ******************** Constructors **************************************
    public Main() {
        CommPortIdentifier portId   = null;
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

        while (portEnum.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
            if (currPortId.getName().equals(PORT)) { portId = currPortId; }
        }
        if (portId == null) {
            System.out.println("Could not find COM port.");
            return;
        }

        try {
            serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);

            // set port parameters
            serialPort.setSerialPortParams(DATA_RATE,
                                           SerialPort.DATABITS_8,
                                           SerialPort.STOPBITS_1,
                                           SerialPort.PARITY_NONE);

            // open the streams
            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));

            // add event listeners
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        NmeaManager.INSTANCE.setOnNmeaEvent(event -> handleNmeaEvent(event));

        while(true) {
            try { Thread.sleep(100); } catch(InterruptedException e) {}
        }
    }


    // ******************** Methods *******************************************
    @Override public synchronized void serialEvent(final SerialPortEvent EVENT) {
        if (EVENT.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                final String[] DATA_ARRAY = input.readLine().replaceAll("\r\n", ";").split(";");
                Arrays.stream(DATA_ARRAY).forEach(NmeaManager.INSTANCE::parse);
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }

    private void handleNmeaEvent(final NmeaEvent EVENT) {
        final String  SENTENCE = EVENT.NMEA_SENTENCE.getName();
        switch(SENTENCE) {
            case "GGA":
                System.out.println(GGA.INSTANCE.toString());
                break;
            case "VTG":
                System.out.println(VTG.INSTANCE.toString());
                break;
            case "GSA":
                System.out.println(GSA.INSTANCE.toString());
                break;
            case "RMC":
                System.out.println(RMC.INSTANCE.toString());
                break;
            case "GSV":
                System.out.println(GSV.INSTANCE.toString());
                break;
        }
    }

    public static void main(String[] args) {
        new Main();
    }
}
