/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.nmea;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by hansolo on 11.11.14.
 */
public enum NmeaManager {
    INSTANCE;

    // NMEA Sentences
    public enum TalkerId {
        GPS("$GP"),
        GLONASS("$GL"),
        GALILEO("$GA"),
        BEIDOU("$GB"),
        GNSS("$GN");

        public final String PREFIX;

        TalkerId(final String PREFIX) {
            this.PREFIX = PREFIX;
        }
    }
    public enum GGA implements NmeaSentence {
        INSTANCE;

        public final String NAME               = "GGA";
        public       String time               = "000000";
        public       int    hour               = 0;
        public       int    minute             = 0;
        public       int    second             = 0;
        public       double latitude           = 0;
        public       char   latitudeIndicator  = ' ';
        public       double longitude          = 0;
        public       char   longitudeIndicator = ' ';
        public       int    quality            = 0;
        public       int    satellitesTracked  = 0;
        public       double hdop               = 0;
        public       double altitude           = 0;

        public String getName() { return NAME; }

        @Override public String toString() {
            return new StringBuilder(NAME)
                .append("\n")
                .append("time               : ").append(time).append(" (").append(hour).append(":").append(minute).append(":").append(second).append(")").append("\n")
                .append("latitude           : ").append(latitude).append("\n")
                .append("latitude indicator : ").append(latitudeIndicator).append("\n")
                .append("longitude          : ").append(longitude).append("\n")
                .append("longitude indicator: ").append(longitudeIndicator).append("\n")
                .append("quality            : ").append(quality).append(" (").append(FixQuality.values()[quality]).append(")").append("\n")
                .append("satellites tracked : ").append(satellitesTracked).append("\n")
                .append("hdop               : ").append(hdop).append("\n")
                .append("altitude           : ").append(altitude).append("\n")
                .append("\n").toString();
        }
    }
    public enum GLL implements NmeaSentence {
        INSTANCE;

        public final String NAME               = "GLL";
        public       double latitude           = 0;
        public       char   latitudeIndicator  = ' ';
        public       double longitude          = 0;
        public       char   longitudeIndicator = ' ';
        public       String time               = "000000";
        public       int    hour               = 0;
        public       int    minute             = 0;
        public       int    second             = 0;
        public       char   status             = ' ';
        public       char   posMode            = ' ';

        public String getName() { return NAME; }

        @Override public String toString() {
            return new StringBuilder(NAME)
                .append("\n")
                .append("latitude           : ").append(latitude).append("\n")
                .append("latitude indicator : ").append(latitudeIndicator).append("\n")
                .append("longitude          : ").append(longitude).append("\n")
                .append("longitude indicator: ").append(longitudeIndicator).append("\n")
                .append("time               : ").append(time).append(" (").append(hour).append(":").append(minute).append(":").append(second).append(")").append("\n")
                .append("status             : ").append(status).append("\n")
                .append("pos mode           : ").append(posMode).append("\n")
                .append("\n").toString();
        }
    }
    public enum GSA implements NmeaSentence {
        INSTANCE;

        public final String NAME = "GSA";
        public       int    fix  = 1;
        public       int[]  prns = {0,0,0,0,0,0,0,0,0,0,0,0};
        public       double pdop = 0;
        public       double hdop = 0;

        public String getName() { return NAME; }

        @Override public String toString() {
            return new StringBuilder(NAME)
                .append("\n")
                .append("fix                : ").append(fix).append(" (").append(Fix.values()[fix - 1]).append(")").append("\n")
                .append("prns               : ")
                .append(prns[0]).append(",")
                .append(prns[1]).append(",")
                .append(prns[2]).append(",")
                .append(prns[3]).append(",")
                .append(prns[4]).append(",")
                .append(prns[5]).append(",")
                .append(prns[6]).append(",")
                .append(prns[7]).append(",")
                .append(prns[8]).append(",")
                .append(prns[9]).append(",")
                .append(prns[10]).append(",")
                .append(prns[11]).append("\n")
                .append("pdop               : ").append(pdop).append("\n")
                .append("hdop               : ").append(hdop).append("\n")
                .append("\n").toString();
        }
    }
    public enum RMC implements NmeaSentence {
        INSTANCE;

        public final String NAME               = "RMC";
        public       String time               = "000000";
        public       int    hour               = 0;
        public       int    minute             = 0;
        public       int    second             = 0;
        public       char   status             = 'V';
        public       double latitude           = 0f;
        public       double longitude          = 0f;
        public       char   latitudeIndicator  = ' ';
        public       char   longitudeIndicator = ' ';
        public       double speedInKph         = 0f;
        public       double speedInKnots       = 0f;
        public       double course             = 0f;
        public       String date               = "010170";
        public       int    day                = 1;
        public       int    month              = 1;
        public       int    year               = 70;
        public       String mode               = "";

        public String getName() { return NAME; }

        @Override public String toString() {
            StringBuilder string = new StringBuilder(NAME)
                .append("\n")
                .append("time               : ").append(time).append(" (").append(hour).append(":").append(minute).append(":").append(second).append(")").append("\n")
                .append("status             : ").append(status).append("\n")
                .append("latitude           : ").append(latitude).append("\n")
                .append("longitude          : ").append(longitude).append("\n")
                .append("latitude indicator : ").append(latitudeIndicator).append("\n")
                .append("longitude indicator: ").append(longitudeIndicator).append("\n")
                .append("speed in kph       : ").append(speedInKph).append("\n")
                .append("speed in knots     : ").append(speedInKnots).append("\n")
                .append("course             : ").append(course).append("\n")
                .append("date               : ").append(date).append(" (").append(day).append(".").append(month).append(".").append(year).append(")").append("\n")
                .append("mode               : ").append(mode).append(" (");
            switch(mode) {
                case "A": string.append("Autonomous Mode)"); break;
                case "D": string.append("Differential Mode)"); break;
                case "E": string.append("Estimated Mode)"); break;
                case "N": string.append("Data not valid)"); break;
                case "S": string.append("Simulated Mode)"); break;
                default : string.append("-)"); break;
            }
            string.append("\n").append("\n");

            return string.toString();
        }
    }
    public enum VTG implements NmeaSentence {
        INSTANCE;

        public final String NAME           = "VTG";
        public       double speedInKph     = 0f;
        public       double speedInKnots   = 0f;
        public       double trueCourse     = 0f;
        public       double magneticCourse = 0f;

        public String getName() { return NAME; }

        @Override public String toString() {
            return new StringBuilder(NAME)
                .append("\n")
                .append("speed in kph       : ").append(speedInKph).append("\n")
                .append("speed in knots     : ").append(speedInKnots).append("\n")
                .append("true course        : ").append(trueCourse).append("\n")
                .append("magnetic course    : ").append(magneticCourse).append("\n")
                .append("\n").toString();
        }
    }
    public enum GSV implements NmeaSentence {
        INSTANCE;

        public final String         NAME                 = "GSV";
        public       int            noOfGsvSentences     = 0;
        public       int            currentSentence      = 0;
        public       int            lastSentence         = 0;
        public       int            noOfSatellitesInView = 0;

        public String getName() { return NAME; }

        @Override public String toString() {
            return new StringBuilder(NAME)
                .append("\n")
                .append("no of gsv sentences: ").append(noOfGsvSentences).append("\n")
                .append("current sentence   : ").append(currentSentence).append("\n")
                .append("last sentence      : ").append(lastSentence).append("\n")
                .append("satellites in view : ").append(noOfSatellitesInView).append("\n")
                .append("\n").toString();
        }
    }

    public enum Fix { NO_FIX, FIX_2D, FIX_3D }
    public enum FixQuality { INVALID, GPS_FIX, DGPS_FIX, PPS_FIX, RTK, RTK_FLOAT, ESTIMATED, MANUAL, SIMULATION }

    // Conversion factor knot -> kph
    public static final double KNOT = 1.852f;

    // Parser definitions
    //private static final String  GGA_HEADER  = "\\$GPGGA|\\$GLGGA|\\$GBGGA|\\$GNGGA|\\$GAGGA";
    private static final String  GGA_HEADER  = new StringBuilder("\\").append(TalkerId.GPS.PREFIX).append(GGA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GLONASS.PREFIX).append(GGA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GALILEO.PREFIX).append(GGA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.BEIDOU.PREFIX).append(GGA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GNSS.PREFIX).append(GGA.INSTANCE.getName()).toString();
    //private static final String  GLL_HEADER  = "\\$GPGLL|\\$GLGLL|\\$GBGLL|\\$GNGLL|\\$GAGLL";
    private static final String  GLL_HEADER  = new StringBuilder("\\").append(TalkerId.GPS.PREFIX).append(GLL.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GLONASS.PREFIX).append(GLL.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GALILEO.PREFIX).append(GLL.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.BEIDOU.PREFIX).append(GLL.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GNSS.PREFIX).append(GLL.INSTANCE.getName()).toString();
    //private static final String  GSA_HEADER  = "\\$GPGSA|\\$GLGSA|\\$GBGSA|\\$GNGSA|\\$GAGSA";
    private static final String  GSA_HEADER  = new StringBuilder("\\").append(TalkerId.GPS.PREFIX).append(GSA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GLONASS.PREFIX).append(GSA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GALILEO.PREFIX).append(GSA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.BEIDOU.PREFIX).append(GSA.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GNSS.PREFIX).append(GSA.INSTANCE.getName()).toString();
    //private static final String  VTG_HEADER  = "\\$GPVTG|\\$GLVTG|\\$GBVTG|\\$GNVTG|\\$GAVTG";
    private static final String  VTG_HEADER  = new StringBuilder("\\").append(TalkerId.GPS.PREFIX).append(VTG.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GLONASS.PREFIX).append(VTG.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GALILEO.PREFIX).append(VTG.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.BEIDOU.PREFIX).append(VTG.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GNSS.PREFIX).append(VTG.INSTANCE.getName()).toString();
    //private static final String  RMC_HEADER  = "\\$GPRMC|\\$GLRMC|\\$GBRMC|\\$GNRMC|\\$GARMC";
    private static final String  RMC_HEADER  = new StringBuilder("\\").append(TalkerId.GPS.PREFIX).append(RMC.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GLONASS.PREFIX).append(RMC.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GALILEO.PREFIX).append(RMC.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.BEIDOU.PREFIX).append(RMC.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GNSS.PREFIX).append(RMC.INSTANCE.getName()).toString();
    //private static final String  GSV_HEADER  = "\\$GPGSV|\\$GLGSV|\\$GBGSV|\\$GNGSV|\\$GAGSV";
    private static final String  GSV_HEADER  = new StringBuilder("\\").append(TalkerId.GPS.PREFIX).append(GSV.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GLONASS.PREFIX).append(GSV.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GALILEO.PREFIX).append(GSV.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.BEIDOU.PREFIX).append(GSV.INSTANCE.getName()).append("|\\")
                                                                      .append(TalkerId.GNSS.PREFIX).append(GSV.INSTANCE.getName()).toString();
    private static final String  GGA_GROUP   = String.join("", "(", GGA_HEADER, ")");
    private static final String  GLL_GROUP   = String.join("", "(", GLL_HEADER, ")");
    private static final String  GSA_GROUP   = String.join("", "(", GSA_HEADER, ")");
    private static final String  VTG_GROUP   = String.join("", "(", VTG_HEADER, ")");
    private static final String  RMC_GROUP   = String.join("", "(", RMC_HEADER, ")");
    private static final String  GSV_GROUP   = String.join("", "(", GSV_HEADER, ")");
    private static final String  FLOAT       = "(\\-?[0-9]*.?[0-9]*)";
    private static final String  INT         = "(\\-?[0-9]*)";
    private static final String  STRING      = "([^,]+)";
    private static final String  CHAR        = "([a-zA-Z]{1})";
    private static final Pattern GGA_PATTERN = Pattern.compile(String.join(",", GGA_GROUP, STRING, FLOAT, CHAR, FLOAT, CHAR, INT, INT, FLOAT, FLOAT, CHAR, FLOAT, CHAR));
    private static final Matcher GGA_MATCHER = GGA_PATTERN.matcher("");
    private static final Pattern GLL_PATTERN = Pattern.compile(String.join(",", GLL_GROUP, FLOAT, CHAR, FLOAT, CHAR, STRING, CHAR));
    private static final Matcher GLL_MATCHER = GLL_PATTERN.matcher("");
    private static final Pattern GSA_PATTERN = Pattern.compile(String.join(",", GSA_GROUP, CHAR, INT, INT, INT, INT, INT, INT, INT, INT, INT, INT, INT, INT, INT, FLOAT, FLOAT));
    private static final Matcher GSA_MATCHER = GSA_PATTERN.matcher("");
    private static final Pattern VTG_PATTERN = Pattern.compile(String.join(",", VTG_GROUP, FLOAT, CHAR, FLOAT, CHAR, FLOAT, CHAR, FLOAT, CHAR));
    private static final Matcher VTG_MATCHER = VTG_PATTERN.matcher("");
    private static final Pattern RMC_PATTERN = Pattern.compile(String.join(",", RMC_GROUP, STRING, CHAR, FLOAT, CHAR, FLOAT, CHAR, FLOAT, FLOAT, STRING, FLOAT, CHAR));
    private static final Matcher RMC_MATCHER = RMC_PATTERN.matcher("");
    private static final Pattern GSV_PATTERN = Pattern.compile(String.join(",", GSV_GROUP, INT, INT, INT));
    private static final Matcher GSV_MATCHER = GSV_PATTERN.matcher("");

    private List<NmeaListener>  listenerList = new CopyOnWriteArrayList<>();


    // ******************** Methods *******************************************
    public void parse(final String SENTENCE) {
        if (SENTENCE.startsWith("$") && SENTENCE.contains(",")) {
            final String HEADER = SENTENCE.substring(SENTENCE.indexOf(",") - 3, SENTENCE.indexOf(","));
            switch (HEADER) {
                case "GGA":
                    parseGGA(SENTENCE);
                    break;
                case "GLL":
                    parseGLL(SENTENCE);
                    break;
                case "GSA":
                    parseGSA(SENTENCE);
                    break;
                case "VTG":
                    parseVTG(SENTENCE);
                    break;
                case "RMC":
                    parseRMC(SENTENCE);
                    break;
                case "GSV":
                    parseGSV(SENTENCE);
                    break;
            }
        }
    }

    private void parseGGA(final String SENTENCE) {
        GGA_MATCHER.reset(SENTENCE);
        while (GGA_MATCHER.find()) {
            GGA.INSTANCE.time               = GGA_MATCHER.group(2).isEmpty() ? "000000" : GGA_MATCHER.group(2);
            GGA.INSTANCE.hour               = Integer.parseInt(GGA.INSTANCE.time.substring(0, 2));
            GGA.INSTANCE.minute             = Integer.parseInt(GGA.INSTANCE.time.substring(2, 4));
            GGA.INSTANCE.second             = Integer.parseInt(GGA.INSTANCE.time.substring(4, 6));
            GGA.INSTANCE.latitudeIndicator  = GGA_MATCHER.group(4).isEmpty() ? 'N' : GGA_MATCHER.group(4).charAt(0);
            GGA.INSTANCE.longitudeIndicator = GGA_MATCHER.group(6).isEmpty() ? 'E' : GGA_MATCHER.group(6).charAt(0);
            GGA.INSTANCE.latitude           = GGA_MATCHER.group(3).isEmpty() ? 0 : degreesMinToDegrees(GGA_MATCHER.group(3)) * (GGA.INSTANCE.latitudeIndicator == 'S' ? -1 : 1);
            GGA.INSTANCE.longitude          = GGA_MATCHER.group(5).isEmpty() ? 0 : degreesMinToDegrees(GGA_MATCHER.group(5)) * (GGA.INSTANCE.longitudeIndicator == 'W' ? -1 : 1);
            GGA.INSTANCE.quality            = GGA_MATCHER.group(7).isEmpty() ? 0 : Integer.parseInt(GGA_MATCHER.group(7)); // 0 = no fix, 1 = fix 2D/3D GPS, 2 = fix DGPS, 6 = estimated fix
            GGA.INSTANCE.satellitesTracked  = GGA_MATCHER.group(8).isEmpty() ? 0 : Integer.parseInt(GGA_MATCHER.group(8)); // 00 ~ 16
            GGA.INSTANCE.hdop               = GGA_MATCHER.group(9).isEmpty() ? 20 : Double.parseDouble(GGA_MATCHER.group(9));
            GGA.INSTANCE.altitude           = isNumeric(GGA_MATCHER.group(10)) ? Double.parseDouble(GGA_MATCHER.group(10)) : 0;
        }
        fireNmeaEvent(new NmeaEvent(this, GGA.INSTANCE));
    }
    private void parseGLL(final String SENTENCE) {
        GLL_MATCHER.reset(SENTENCE);
        while (GLL_MATCHER.find()) {
            GLL.INSTANCE.latitudeIndicator  = GLL_MATCHER.group(3).isEmpty() ? 'N' : GLL_MATCHER.group(3).charAt(0);
            GLL.INSTANCE.longitudeIndicator = GLL_MATCHER.group(5).isEmpty() ? 'E' : GLL_MATCHER.group(5).charAt(0);
            GLL.INSTANCE.latitude           = GLL_MATCHER.group(2).isEmpty() ? 0 : degreesMinToDegrees(GLL_MATCHER.group(2)) * (GLL.INSTANCE.latitudeIndicator == 'S' ? -1 : 1);
            GLL.INSTANCE.longitude          = GLL_MATCHER.group(4).isEmpty() ? 0 : degreesMinToDegrees(GLL_MATCHER.group(4)) * (GLL.INSTANCE.longitudeIndicator == 'W' ? -1 : 1);
            GLL.INSTANCE.time               = GLL_MATCHER.group(6).isEmpty() ? "000000" : GLL_MATCHER.group(6);
            GLL.INSTANCE.hour               = Integer.parseInt(GLL.INSTANCE.time.substring(0, 2));
            GLL.INSTANCE.minute             = Integer.parseInt(GLL.INSTANCE.time.substring(2, 4));
            GLL.INSTANCE.second             = Integer.parseInt(GLL.INSTANCE.time.substring(4, 6));
            GLL.INSTANCE.status             = GLL_MATCHER.group(7).isEmpty() ? 'V' : GLL_MATCHER.group(7).charAt(0);
        }
        fireNmeaEvent(new NmeaEvent(this, GLL.INSTANCE));
    }
    private void parseGSA(final String SENTENCE) {
        GSA_MATCHER.reset(SENTENCE);
        while (GSA_MATCHER.find()) {
            GSA.INSTANCE.fix      = GSA_MATCHER.group(3).isEmpty() ? 1 : Integer.parseInt(GSA_MATCHER.group(3));
            GSA.INSTANCE.prns[0]  = GSA_MATCHER.group(4).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(4));
            GSA.INSTANCE.prns[1]  = GSA_MATCHER.group(5).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(5));
            GSA.INSTANCE.prns[2]  = GSA_MATCHER.group(6).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(6));
            GSA.INSTANCE.prns[3]  = GSA_MATCHER.group(7).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(7));
            GSA.INSTANCE.prns[4]  = GSA_MATCHER.group(8).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(8));
            GSA.INSTANCE.prns[5]  = GSA_MATCHER.group(9).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(9));
            GSA.INSTANCE.prns[6]  = GSA_MATCHER.group(10).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(10));
            GSA.INSTANCE.prns[7]  = GSA_MATCHER.group(11).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(11));
            GSA.INSTANCE.prns[8]  = GSA_MATCHER.group(12).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(12));
            GSA.INSTANCE.prns[9]  = GSA_MATCHER.group(13).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(13));
            GSA.INSTANCE.prns[10] = GSA_MATCHER.group(14).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(14));
            GSA.INSTANCE.prns[11] = GSA_MATCHER.group(15).isEmpty() ? 0 : Integer.parseInt(GSA_MATCHER.group(15));
            GSA.INSTANCE.pdop     = GSA_MATCHER.group(16).isEmpty() ? 0 : Double.parseDouble(GSA_MATCHER.group(16));
            GSA.INSTANCE.hdop     = GSA_MATCHER.group(17).isEmpty() ? 0 : Double.parseDouble(GSA_MATCHER.group(17));
        }
        fireNmeaEvent(new NmeaEvent(this, GSA.INSTANCE));
    }
    private void parseVTG(final String SENTENCE) {
        VTG_MATCHER.reset(SENTENCE);
        while (VTG_MATCHER.find()) {
            VTG.INSTANCE.trueCourse     = VTG_MATCHER.group(2).isEmpty() ? 0 : Double.parseDouble(VTG_MATCHER.group(2));
            VTG.INSTANCE.magneticCourse = VTG_MATCHER.group(4).isEmpty() ? 0 : Double.parseDouble(VTG_MATCHER.group(4));
            VTG.INSTANCE.speedInKnots   = VTG_MATCHER.group(6).isEmpty() ? 0 : Double.parseDouble(VTG_MATCHER.group(6));
            VTG.INSTANCE.speedInKph     = VTG_MATCHER.group(8).isEmpty() ? 0 : Double.parseDouble(VTG_MATCHER.group(8));
        }
        fireNmeaEvent(new NmeaEvent(this, VTG.INSTANCE));
    }
    private void parseRMC(final String SENTENCE) {
        RMC_MATCHER.reset(SENTENCE);
        while (RMC_MATCHER.find()) {
            RMC.INSTANCE.time               = RMC_MATCHER.group(2).isEmpty() ? "000000" : RMC_MATCHER.group(2);
            RMC.INSTANCE.hour               = Integer.parseInt(RMC.INSTANCE.time.substring(0, 2));
            RMC.INSTANCE.minute             = Integer.parseInt(RMC.INSTANCE.time.substring(2, 4));
            RMC.INSTANCE.second             = Integer.parseInt(RMC.INSTANCE.time.substring(4, 6));
            RMC.INSTANCE.status             = RMC_MATCHER.group(3).isEmpty() ? 'V' : RMC_MATCHER.group(3).charAt(0);
            RMC.INSTANCE.latitudeIndicator  = RMC_MATCHER.group(5).isEmpty() ? 'N' : RMC_MATCHER.group(5).charAt(0);
            RMC.INSTANCE.longitudeIndicator = RMC_MATCHER.group(7).isEmpty() ? 'E' : RMC_MATCHER.group(5).charAt(0);
            RMC.INSTANCE.latitude           = RMC_MATCHER.group(4).isEmpty() ? 0 : degreesMinToDegrees(RMC_MATCHER.group(4)) * (RMC.INSTANCE.latitudeIndicator == 'S' ? -1 : 1);
            RMC.INSTANCE.longitude          = RMC_MATCHER.group(6).isEmpty() ? 0 : degreesMinToDegrees(RMC_MATCHER.group(6)) * (RMC.INSTANCE.longitudeIndicator == 'W' ? -1 : 1);
            RMC.INSTANCE.speedInKnots       = RMC_MATCHER.group(8).isEmpty() ? 0 : Double.parseDouble(RMC_MATCHER.group(8));
            RMC.INSTANCE.speedInKph         = RMC.INSTANCE.speedInKnots / KNOT;
            RMC.INSTANCE.course             = RMC_MATCHER.group(9).isEmpty() ? 0 : Double.parseDouble(RMC_MATCHER.group(9));
            RMC.INSTANCE.date               = RMC_MATCHER.group(10).isEmpty() ? "010170" : RMC_MATCHER.group(10);
            RMC.INSTANCE.day                = Integer.parseInt(RMC.INSTANCE.date.substring(0, 2));
            RMC.INSTANCE.month              = Integer.parseInt(RMC.INSTANCE.date.substring(2, 4));
            RMC.INSTANCE.year               = Integer.parseInt(RMC.INSTANCE.date.substring(4, 6));
            RMC.INSTANCE.mode               = RMC_MATCHER.group(12).isEmpty() ? "N" : (RMC_MATCHER.group(12).contains("*") ? RMC_MATCHER.group(12)
                                                                                                                          .substring(0, RMC_MATCHER.group(12)
                                                                                                                                                   .lastIndexOf(
                                                                                                                                                       "*")) : RMC_MATCHER.group(
                12)); // N = Data not valid, A = Autonomous mode, D = Differential mode, E = Estimated mode
        }
        fireNmeaEvent(new NmeaEvent(this, RMC.INSTANCE));
    }
    private void parseGSV(final String SENTENCE) {
        GSV_MATCHER.reset(SENTENCE);
        while (GSV_MATCHER.find()) {
            GSV.INSTANCE.noOfGsvSentences     = GSV_MATCHER.group(2).isEmpty() ? 0 : Integer.parseInt(GSV_MATCHER.group(2));
            GSV.INSTANCE.lastSentence         = GSV.INSTANCE.currentSentence;
            GSV.INSTANCE.currentSentence      = GSV_MATCHER.group(3).isEmpty() ? 0 : Integer.parseInt(GSV_MATCHER.group(3));
            GSV.INSTANCE.noOfSatellitesInView = GSV_MATCHER.group(4).isEmpty() ? 0 : Integer.parseInt(GSV_MATCHER.group(4));
        }
        fireNmeaEvent(new NmeaEvent(this, GSV.INSTANCE));
    }


    // ******************** Helper Methods ************************************
    public double degreesMinToDegrees(final String DD_MM) {
        // This methods accept all strings of the format
        // DDDMM.MMMM
        // DDDMM
        // MM.MMMM
        // MM

        // check first character, rest is checked by parseInt/parseFloat
        int len = DD_MM.length();
        if (len <= 0 || DD_MM.charAt(0) == '-') throw new NumberFormatException();

        int dotPosition = DD_MM.indexOf('.');
        if (dotPosition < 0) dotPosition = len;

        int degrees;
        double minutes;
        if (dotPosition > 2) {
            degrees = Integer.parseInt(DD_MM.substring(0, dotPosition - 2));
            if (DD_MM.charAt(dotPosition - 2) == '-') throw new NumberFormatException();
            minutes = Double.parseDouble(DD_MM.substring(dotPosition - 2));
        } else {
            degrees = 0;
            minutes = Double.parseDouble(DD_MM);
        }
        return degrees + minutes * (1.0 / 60.0);
    }

    private boolean isNumeric(final String STRING) {
        if (null == STRING || STRING.isEmpty()) return false;
        int length = STRING.length();
        for (int x = 0; x < length; x++) {
            final char c = STRING.charAt(x);
            if (x == 0 && (c == '-')) continue;      // negative
            if ((c >= '0') && (c <= '9')) continue;  // 0 - 9
            if (c == '.') continue;                  // double or double values
            return false;                            // invalid
        }
        return true;                                 // valid
    }


    // ******************** Event Handling ************************************
    public final void setOnNmeaEvent(final NmeaListener LISTENER) { addListener(LISTENER); }
    public final void addListener(final NmeaListener LISTENER) {
        if (listenerList.contains(LISTENER)) return;
        listenerList.add(LISTENER);
    }
    public final void removeListener(final NmeaListener LISTENER) {
        if (listenerList.contains(LISTENER)) listenerList.remove(LISTENER);
    }
    public final List<NmeaListener> getListeners() { return listenerList; }

    public void fireNmeaEvent(final NmeaEvent EVENT) { listenerList.forEach(listener -> listener.onEvent(EVENT)); }


    // ******************** Inner Classes *************************************
    public static class Satellite {
        public int prn;
        public int elevation;
        public int azimuth;
        public int snr;

        public Satellite(final int PRN, final int ELEVATION, final int AZIMUTH, final int SNR) {
            prn       = PRN;
            elevation = ELEVATION;
            azimuth   = AZIMUTH;
            snr       = SNR;
        }

        @Override public String toString() {
            return new StringBuilder("prn      : ").append(prn).append("\n")
                                                   .append("elevation: ").append(elevation).append("\n")
                                                   .append("azimuth  : ").append(elevation).append("\n")
                                                   .append("snr      : ").append(elevation).append("\n").toString();
        }
    }
}
