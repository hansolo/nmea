/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.nmea;

import java.util.EventObject;


/**
 * Created by hansolo on 27.03.15.
 */
public class NmeaEvent extends EventObject {
    public final NmeaSentence NMEA_SENTENCE;


    // ******************* Constructors ***************************************
    public NmeaEvent(final Object SOURCE, final NmeaSentence NMEA_SENTENCE) {
        super(SOURCE);
        this.NMEA_SENTENCE = NMEA_SENTENCE;
    }
}
