NMEA Parser

This is a simple NMEA parser that is able to parse GPS, BEIDOU, GLONASS, GALILEO and GNSS sentences.

The native libs to access the com port on the different platforms can be found in the natives folder.

Project needs Java 8.